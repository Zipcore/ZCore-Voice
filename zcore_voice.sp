#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Z-Core: Voice"
#define PLUGIN_VERSION "1.0"
#define PLUGIN_DESCRIPTION "Player listen override forward and native"
#define PLUGIN_URL "zipcore.net"

#include <sourcemod>
#include <sdktools>
#include <smlib>
#include <zcore/zcore_voice>

public Plugin myinfo = 
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
}

/* Forwards */

Handle g_OnListenOverride;

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	RegPluginLibrary("zcore-voice");
	
	CreateNative("ZCore_Voice_SetListenOverride", Native_SetListenOverride);
	
	g_OnListenOverride = CreateGlobalForward("ZCore_Voice_OnListenOverride", ET_Event, Param_Cell, Param_Cell, Param_Cell, Param_Cell);
	
	return APLRes_Success;
}

public void OnPluginStart()
{
	CreateConVar("zcore_voice_version", PLUGIN_VERSION, "Z-Core: Voice version", FCVAR_DONTRECORD|FCVAR_PLUGIN|FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
}

public int Native_SetListenOverride(Handle plugin, int numParams)
{
	int receiver = GetNativeCell(1);
	int sender = GetNativeCell(2);
	bool listen = GetNativeCell(3);
	int immunity = GetNativeCell(4);
	
	Action result;
	
	Call_StartForward(g_OnListenOverride);
	Call_PushCell(receiver);
	Call_PushCell(sender);
	Call_PushCell(listen);
	Call_PushCell(immunity);
	
	Call_Finish(result);
	
	if(result == Plugin_Stop || result == Plugin_Handled)
	{
		NewListenOverride(receiver, sender, false)
		return false;
	}
	
	return NewListenOverride(receiver, sender, listen);
}

bool NewListenOverride(int receiver, int sender, bool listen)
{
	if(listen)
	{
		SetListenOverride(receiver, sender, Listen_Yes);
		return listen;
	}
	
	SetListenOverride(receiver, sender, Listen_No);
	return listen;
}